FROM busybox
RUN mkdir /var/www/html
ADD html.tgz /var/www
EXPOSE 80
VOLUME /var/www/html
CMD httpd -h /var/www/html;tail -f /dev/null
